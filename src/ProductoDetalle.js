import React, { Component }  from 'react';
import {Link} from "react-router-dom";
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';


const styles = {
    cards:{
        width:'70%',
        margin:'auto',
        marginTop:'40px'
    },

    image:{
        height:'200px',
        width: 'auto',
        display: 'block',
        margin: 'auto',
    }

}

class ProductoDetalle extends Component{
    constructor(props){
        super(props)
        this.state={
            mensaje:""
        }
    }
    handleClick = ()=>{
        this.setState({
            mensaje:"Gracias por su compra"
        })
    }
    render(){
        return (
            
            <Card style={styles.cards}>
                <Container>
                    <Row>
                        <Col>
                        
                        <Card.Img src={this.props.data.photo_url} style={styles.image}/>
                        </Col>

                        <Col xs={8}>
                        <Card.Body>
                            <Card.Title>{this.props.data.name}</Card.Title>
                            <Card.Subtitle className="mb-2 text-muted">
                                ${this.props.data.price}
                            </Card.Subtitle>
                            
                            <Button variant="primary"  onClick={this.handleClick}>Comprar</Button>
                            {
                                this.props.buttons &&
                                <Button as={Link} to={'/producto/'+this.props.data.id}  variant="outline-primary" >Ver detalle</Button>
                            }
                            {this.state.mensaje}
                        
                        </Card.Body>
                        </Col>


                    </Row>


              </Container>

            </Card>
        )  
    }
}
export default ProductoDetalle;