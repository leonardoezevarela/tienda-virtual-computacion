import firebase from "firebase";
const firebaseConfig = {
        apiKey: "AIzaSyBbne2Ot5wvsp4uBJi5XJD_j-WjNm_7MJA",
        authDomain: "reactutn-f4fde.firebaseapp.com",
        databaseURL: "https://reactutn-f4fde.firebaseio.com",
        projectId: "reactutn-f4fde",
        storageBucket: "reactutn-f4fde.appspot.com",
        messagingSenderId: "470029852680",
        appId: "1:470029852680:web:2713e7baa5270822a4b1bd"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);
      const db = firebase.firestore();
      db.settings({
              timestampsInSnapshot: true
      });
      firebase.auth = firebase.auth();
      firebase.db=db;
      export default firebase;